//= require_tree .

var js = js || {},
  body = $('body'),
  doc = $(document);

js.main = {
  init: function () {
    this.siteJS();
    this.linksExternal();
  },
  siteJS: function () {
    $("a[href='#page_about']").click(function(e){
      $('body').addClass('page-about');
      e.preventDefault();
    });

    $("#close_about").click(function(e){
      $('body').removeClass('page-about');
      e.preventDefault();
    });

    $("a[href='#page_contact']").click(function(e){
      $('body').addClass('page-contact');
      e.preventDefault();
    });

    $("#close_contact").click(function(e){
      $('body').removeClass('page-contact');
      e.preventDefault();
    });

    var $things = $('.site-page .site-section:nth-child(2)');

    $things.waypoint(function(direction) {
      if (direction === 'down') {
        $('.site-header').addClass('scrolled');
      }
    }, {
      offset: '0'
    });

    $things.waypoint(function(direction) {
      if (direction === 'up') {
        $('.site-header').removeClass('scrolled');
      }
    }, {
      offset: '25%'
    });

    document.getElementById("hero-video").defaultPlaybackRate = 2.0;
  },
  linksExternal: function () {
    $("a[href^='http://']").attr("target", "_blank");
    $("a[href^='https://']").attr("target", "_blank");
  },
};

doc.ready(function () {
  js.main.init();
});
